.include "include/syscall.s"
.section .bss

.equ bufsize, 4 
.lcomm buffer, bufsize
.equ NL, '\n'

.section .text
.globl _start

# Exit with return status %ebx
exit:
	# %eax := ($SYS_EXIT = 1)
	  xor %eax, %eax
	  inc %eax 
	int $0x80

read_error:
	mov %eax, %ebx
	neg %ebx
	jmp exit

# Write (%edx + 1) bytes of $buffer into stdout and exit.
newline_found:
	inc %ebx
	inc %edx

	mov $SYS_WRITE, %eax
	int $0x80

	dec %ebx # %ebx: 1 -> 0
	jmp exit

_start:
	mov $buffer, %ecx # %ecx will not be changed
                          # it is used for both read(2) and write(2)

read_buffer_loop:
	# Read $bufsize bytes into $buffer
	  mov $SYS_READ, %eax
	  xor %ebx, %ebx
	  mov $bufsize, %edx
	int $0x80

	# Check return value
	cmp $0, %eax
	je exit       # No data is okay (exit 0)
	jl read_error # Negative return value is bad.

	# Now, we have %eax != 0 bytes of data in $buffer.
	# %edx is index variable
	xor %edx, %edx

scan_nl_loop:
	mov %eax, %ebp
	  movb (%ecx, %edx, 1), %al
	  cmp $NL, %al
	  je newline_found
	mov %ebp, %eax

	inc %edx
	cmp %edx, %eax
	jne scan_nl_loop
	
	inc %ebx # %ebx := 1

	mov $SYS_WRITE, %eax
	int $0x80

	jmp read_buffer_loop
