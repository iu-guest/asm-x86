.include "include/syscall.s"
.section .bss
.equ bufsize, 100
.lcomm buffer, bufsize

.section .data
errmsg:
	.ascii "Life is pain.\n\0"
.equ errmsglen, . - errmsg

.section .text

# NORETURN 
# Calling convention:
#   %ebx -- exit status
exit1:
	dec %ebx
	mov $SYS_EXIT, %eax
	int $0x80

nodata:
	mov $SYS_WRITE, %eax
	mov $2, %ebx
	mov $errmsg, %ecx
	mov $errmsglen, %edx
	int $0x80

	jmp exit1

.globl _start
_start:
	# Read into buffer from stdin
	mov $SYS_READ, %eax
	xor %ebx, %ebx # ebx = 0 (stdin)
       	mov $buffer, %ecx
	mov $bufsize, %edx
	int $0x80

	cmp $0, %eax
	je nodata

	mov %eax, %edx
	# Write that data back
	mov $SYS_WRITE, %eax
	mov $1, %ebx
	int $0x80

	jmp exit1
